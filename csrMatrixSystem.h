#ifndef CSR_MATRIX_SYSTEM_H
#define CSR_MATRIX_SYSTEM_H

#include "csrMatrix.h"
#include "vector.h"

typedef struct {
	MAT* A;
	double* x;
	double* b;
} LinearSystem;

LinearSystem* criarSistemaComSolUnitaria(MAT* A);

void freeLinearSystem(LinearSystem *ls);

void printLinearSystem(LinearSystem *ls);

// A x v
void produtoMatVet(MAT* A, double* v, double* resultado);



#endif
