########################## Makefile ##########################
all: solve
solve:
	gcc -g *.c -o solve -lm -O0
clean:
	rm -rf *.o solve
