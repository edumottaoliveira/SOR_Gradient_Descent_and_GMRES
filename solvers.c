#include "solvers.h"
#include "util.h"
#include <math.h>
#include <time.h>

/*----------------------------------------------------------------------------
 * SOR
 *--------------------------------------------------------------------------*/
// Para matrizes simetricas e positivo definidas

void SOR(LinearSystem* ls, int iMax, double w, double tolerancia, char* nomeMatriz) {

	char nomeArquivo[100];
	sprintf(nomeArquivo, "./plot/sor_%s.dat", nomeMatriz);
	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");
	clock_t start = clock();

	printf("Resolvendo pelo SOR...\n");

	int n = ls->A->n;
	MAT* A = ls->A;
	double* b = ls->b;
	double* x = ls->x;
	double* xOld;
	int linha, indiceProxLinha, i, col, k;
	double residuo, soma, valorDiagonal, normaResiduo;

	normaResiduo = normaEuclidiana(b, n);

	for (i = 0; i < iMax; i++) {

		linha = 0;
		k = 0;

		xOld = copiarVetor(x, n);

		while (k < A->nz) {

			indiceProxLinha = A->IA[linha+1];
			soma = 0.0;
			valorDiagonal = 0.0;

			while (k < indiceProxLinha) {
				col = A->JA[k];
					valorDiagonal = A->AA[k];
				if (linha == col) {
				}
				else {
					soma += A->AA[k] * x[col];
				}
				k++;
			}
			x[linha] = (1-w)*x[linha] + w*(1/valorDiagonal)*(b[linha] - soma);
			linha++;
		}

		residuo = maxSubtracaoVetVet(x, xOld, n) / normaInfinito(x, n);
		freeVetor(xOld);

		//if ((i) % 100 == 0) {
			normaResiduo = residuo;
			fprintf(arquivo, "%d %0.8lf %0.12lf\n", i, log(normaResiduo), normaResiduo);
		//}

		//printf("r = %lf\n", residuo);
		if (residuo < tolerancia) {
			break;
		}
	}

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, i );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, i);
	fclose(arquivo);

	printf("Resolvido em %d iteracoes.\n", i);
}


/*----------------------------------------------------------------------------
 * GC
 *--------------------------------------------------------------------------*/

void CG(LinearSystem* ls, int kMax, double tolerancia, char* nomeMatriz) {
	clock_t start = clock();
	char nomeArquivo[100];
	sprintf(nomeArquivo, "./plot/gc_%s.dat", nomeMatriz);
	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");

	printf("Resolvendo pelo Gradientes Conjugados...\n\n");
	MAT* A = ls->A;
	int n = A->n;
	double* x = ls->x;
	double* b = ls->b;

	double* residuo = copiarVetor(b, n);
	double* v = copiarVetor(b, n);
	double* produtoAv = alocarVetor(n);

	double deltaNovo = normaEuclidianaAoQuadrado(residuo, n);
	double deltaVelho = deltaNovo;
	double alpha, beta, normaResiduo, limiteErro = tolerancia * tolerancia * deltaVelho;

	normaResiduo = normaEuclidiana(residuo, n);

	int k = 0;
	do {
		produtoMatVet(A, v, produtoAv);

		alpha = deltaNovo / produtoEscalarVetVet(v, produtoAv, n);

		somarComProdutoEscalarVet(x, alpha, v, n); // x = x + alpha * v
		somarComProdutoEscalarVet(residuo, (-alpha), produtoAv, n);

		deltaVelho = deltaNovo;
		deltaNovo = normaEuclidianaAoQuadrado(residuo, n);

		beta = deltaNovo/deltaVelho;

		produtoVetorPorConstante(v, beta, v, n);
		somarVetComVet(v, residuo, v, n);

		if ((k) % 10 == 0) {
			normaResiduo = normaEuclidiana(residuo, n);
			fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", k, log(normaResiduo), normaResiduo);
		}
		//printf("r(%d) = %0.12lf  log = %0.8lf\n", k, normaEuclidiana(residuo, n), log(normaEuclidiana(residuo, n)) );
		k++;
	} while (k < kMax && deltaNovo > limiteErro);

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, k );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, k);

	fclose(arquivo);
	//for (int i = 0; i <= k; i++)
	//	printf("r(%d)\t= %lf\n", i, listaNormaResiduos[i]);

	freeVetor(produtoAv);
	freeVetor(residuo);
	freeVetor(v);

}



/*----------------------------------------------------------------------------
 * GMRES
 *--------------------------------------------------------------------------*/

void printMatrix(double** H, int nVet, int tam, char* nome) {
	printf("\n%s = \n\t", nome);
	for (int i = 0; i < tam; i++) {
		for (int j = 0; j < nVet; j++){
			printf("%lf ", H[j][i]);
		}
		printf("\n\t");
	}
}


void GMRES_Restart(LinearSystem* ls, int iMax, int lMax, double tolerancia, char* nomeMatriz) {
	clock_t start = clock();
	char nomeArquivo[100];
	sprintf(nomeArquivo, "./plot/gmres_%d_%s.dat", iMax, nomeMatriz);
	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");



	printf("Resolvendo pelo GMRES...\n\n");
	MAT* A = ls->A;
	int n = A->n;
	double* x = ls->x;
	double* b = ls->b;
	// U(n x iMax+1), um vetor de tam n para cada interacao +1
	double** U = (double**) malloc( (iMax + 1) * sizeof(double*));
	for (int k = 0; k < iMax + 1; k++) {
		U[k] = alocarVetor(n);
	}
	// H(iMax x iMax+1), um vetor de tam iMax+1 para cada iteracao
	double** H = (double**) malloc( iMax * sizeof(double*));
	for (int k = 0; k < iMax; k++) {
		H[k] = alocarVetor(iMax+1);
	}

	double* y = alocarVetor(iMax);
	double* c = alocarVetor(iMax);
	double* s = alocarVetor(iMax);
	double* e = alocarVetor(iMax+1);

	double* residuo = copiarVetor(b,n);
	double* vetorUnitario = gerarVetorUnitario(n);
	double normaResiduo;


	double rho, epsilon = tolerancia * normaEuclidiana(b, n);
	int i, l = 0,  numTotalIter = 0;

	normaResiduo = normaEuclidiana(residuo,n);
	fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", numTotalIter, log(normaResiduo), normaResiduo);

	do {
			//printf("l = %d\n", l);
			produtoMatVet(A, x, U[0]);
			subtrairVetDeVet(b, U[0], U[0], n);

			e[0] = normaEuclidiana(U[0], n);
			dividirVetorPorConstante(U[0], e[0], n);
			rho = e[0];

			i = 0;
			while (rho > epsilon && i < iMax) {
				produtoMatVet(A, U[i], U[i+1]); // U[i+1] = A x U[i]

				// Gram - Schmidt
				for (int j = 0; j <= i; j++) {
					H[i][j] = produtoEscalarVetVet(U[i+1], U[j], n); // Iteracao elem j do H atual
					somarComProdutoEscalarVet(U[i+1], -H[i][j], U[j], n);
				}

				H[i][i+1] = normaEuclidiana(U[i+1], n);
				dividirVetorPorConstante(U[i+1], H[i][i+1], n);

				//Algoritmo QR
				for (int j = 0; j < i; j++) {
					double temp = H[i][j];
					H[i][j] =  c[j] * temp + s[j] * H[i][j+1];
					H[i][j+1] = (-s[j]) * temp + c[j] * H[i][j+1];
				}
				double r = sqrt( pow(H[i][i], 2) + pow(H[i][i+1], 2));

				c[i] = H[i][i] / r;
				s[i] = H[i][i+1] / r;
				H[i][i] = r;
				H[i][i+1] = 0.0;

				e[i+1] = (-s[i]) * e[i];
				e[i] = c[i] * e[i];

				rho = fabs(e[i+1]);

				//printf("rho %d = %lf\n", i, rho);
				//if (numTotalIter % 100 == 0) {


				//fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", numTotalIter, log(rho), rho);
				///}
				numTotalIter++;
				i++;
			}

			i = i - 1;
			for (int j = i; j >= 0; j--) {
				double sum = 0.0;
				for (int l = j+1; l <= i; l++) {
					 sum += H[l][j] * y[l];
				}
				y[j] = (e[j] - sum) / H[j][j];
			}

			//printf("Computando x ...\n");
			for (int j = 0; j <= i; j++) {
				somarComProdutoEscalarVet(x, y[j], U[j], n);
			}

			subtrairVetDeVet(vetorUnitario, x, residuo, n);
			normaResiduo = normaEuclidiana(residuo, n);
			fprintf(arquivo, "%d\t%0.8lf\t%0.12lf\n", numTotalIter, log(normaResiduo), normaResiduo);

			l++;
	} while (rho > epsilon && l < lMax);

	printf("Resolvido em %d iteracoes.\n", numTotalIter);
	// Limpa memoria
	for (int k = 0; k < iMax+1; k++) {
		freeVetor(U[k]);
	}
	FREE(U);
	for (int k = 0; k < iMax; k++) {
		freeVetor(H[k]);
	}
	FREE(H);
	freeVetor(residuo);
	freeVetor(vetorUnitario);
	freeVetor(y);
	freeVetor(c);
	freeVetor(s);
	freeVetor(e);

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter);
	fclose(arquivo);
}
















void GMRES(LinearSystem* ls, int iMax, int lMax, double tolerancia) {

	printf("Resolvendo pelo GMRES...\n\n");

	MAT* A = ls->A;
	int n = A->n;
	double* x = ls->x;
	double* b = ls->b;

	// U(n x iMax+1), um vetor de tam n para cada interacao +1
	double** U = (double**) malloc( (iMax + 1) * sizeof(double*));
	U[0] = copiarVetor(b, n);
	for (int k = 1; k < iMax + 1; k++) {
		U[k] = alocarVetor(n);
	}
	// H(iMax x iMax+1), um vetor de tam iMax+1 para cada iteracao
	double** H = (double**) malloc( iMax * sizeof(double*));
	for (int k = 0; k < iMax; k++) {
		H[k] = alocarVetor(iMax+1);
	}

	double* c = alocarVetor(iMax);
	double* s = alocarVetor(iMax);

	double* e = alocarVetor(iMax+1);
	e[0] = normaEuclidiana(U[0], n);
	dividirVetorPorConstante(U[0], e[0], n);

	double rho = e[0];
	double epsilon = tolerancia * normaEuclidiana(b, n);
	printf("epsilon = %lf\n",epsilon);

	int i = 0;
	while (rho > epsilon && i < iMax) {
		//printf("----%d----\n", i);
		produtoMatVet(A, U[i], U[i+1]); // U[i+1] = A x U[i]

		// Gram - Schmidt

		//  < OU <= ?????????????
		for (int j = 0; j <= i; j++) {

			H[i][j] = produtoEscalarVetVet(U[i+1], U[j], n); // Iteracao elem j do H atual
			somarComProdutoEscalarVet(U[i+1], -H[i][j], U[j], n);
		}

		H[i][i+1] = normaEuclidiana(U[i+1], n);
		dividirVetorPorConstante(U[i+1], H[i][i+1], n);

		//Algoritmo QR
		for (int j = 0; j < i; j++) {
			double temp = H[i][j];
			H[i][j] =  c[j] * temp + s[j] * H[i][j+1];
			H[i][j+1] = (-s[j]) * temp + c[j] * H[i][j+1];
		}
		//printMatrix(H, iMax, iMax+1, "H");
		//printMatrix(U, iMax+1, n, "U");

		double r = sqrt( pow(H[i][i], 2) + pow(H[i][i+1], 2));

		c[i] = H[i][i] / r;
		s[i] = H[i][i+1] / r;
		H[i][i] = r;
		H[i][i+1] = 0.0;

		e[i+1] = (-s[i]) * e[i];
		e[i] = c[i] * e[i];

		rho = fabs(e[i+1]);
		printf("rho %d = %lf\n", i, rho);
		i++;
	}

	i = i - 1;

	double* y = alocarVetor(iMax);

	for (int j = i; j >= 0; j--) {
		double sum = 0.0;
		for (int l = j+1; l <= i; l++) {
			 sum += H[l][j] * y[l];
		}
		y[j] = (e[j] - sum) / H[j][j];
	}

	printf("Computando x ...\n");
	for (int j = 0; j <= i; j++) {
		somarComProdutoEscalarVet(x, y[j], U[j], n);
	}

	// Limpa memoria
	for (int k = 0; k < iMax+1; k++) {
		freeVetor(U[k]);
	}

	FREE(U);
	for (int k = 0; k < iMax; k++) {
		freeVetor(H[k]);
	}
	FREE(H);

	freeVetor(c);
	freeVetor(s);
	freeVetor(e);
	freeVetor(y);
}

/*----------------------------------------------------------------------------
 * LCD
 *--------------------------------------------------------------------------*/

void LCD_Restart(LinearSystem* ls, int kMax, int lMax, double tolerancia, char* nomeMatriz) {
	char nomeArquivo[100];
	sprintf(nomeArquivo, "./plot/lcd_%d_%s.dat", kMax, nomeMatriz);
	printf("salvando em: %s\n", nomeArquivo);
	FILE* arquivo = fopen(nomeArquivo, "w");
	clock_t start = clock();


	printf("Resolvendo pelo LCD...\n\n");
	int n = ls->A->n;
	MAT* A = ls->A;
	double* b = ls->b;
	double* x = ls->x;

	double** P = (double**) malloc( (kMax+1) * sizeof(double*));
	double** Q = (double**) malloc( (kMax+1) * sizeof(double*));
	for (int i = 0; i < kMax+1; i++) {
		P[i] = alocarVetor(n);
		Q[i] = alocarVetor(n);
	}
	double* listaNormaResiduos = alocarVetor(kMax*lMax);
	double alpha, beta;

	double* residuo = copiarVetor(b,n); // r0 = b - Ax
	double normaResiduo = normaEuclidiana(b, n); //ro
	double epsilon = tolerancia * normaResiduo; //limite

	int numTotalIter = 0;

	int k, l = 0;
	copiarValores(residuo, P[0], n);

	while (normaResiduo > epsilon && l < lMax) {
		//printf("l = %d\n", l);
		produtoMatVet(A, P[0], Q[0]); // Q[0] = A x P[0]

		k = 0;
		while (normaResiduo >= epsilon && k < kMax) {
			alpha = produtoEscalarVetVet(P[k], residuo, n);
			alpha /= produtoEscalarVetVet(P[k], Q[k], n);

			somarComProdutoEscalarVet(x, alpha, P[k], n); // x = x + alpha*P[k]
			somarComProdutoEscalarVet(residuo, (-alpha), Q[k], n); // r = r - alpha*Q[k]

			copiarValores(residuo, P[k+1], n);
			produtoMatVet(A, P[k+1], Q[k+1]);

			for (int j = 0; j <= k; j++) {
				beta = - produtoEscalarVetVet(P[j], Q[k+1], n);
				beta /= produtoEscalarVetVet(P[j], Q[j], n);

				somarComProdutoEscalarVet(P[k+1], beta, P[j], n); // P[k+1] = P[k+1] + beta * P[j]
				somarComProdutoEscalarVet(Q[k+1], beta, Q[j], n); // Q[k+1] = Q[k+1] + beta * Q[j]
			}

			normaResiduo = normaEuclidiana(residuo, n);

			//printf("residuo: %0.8lf\n", normaResiduo);
			//if ((numTotalIter) % 100 == 0) {
				fprintf(arquivo, "%d %0.8lf %0.12lf\n", numTotalIter, log(normaResiduo), normaResiduo);
			//}
			numTotalIter++;
			k++;
		}
		copiarValores(residuo, P[0], n);
		l++;
	}


	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	fprintf(arquivo, "Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter );
	printf("Resolvido em %0.2lf segundos (%d iteracoes).\n", seconds, numTotalIter);
	fclose(arquivo);

	freeVetor(residuo);
	// Clean P and Q
	for (int i = 0; i < kMax+1; i++) {
		freeVetor(P[i]);
		freeVetor(Q[i]);
	}
	FREE(P);
	FREE(Q);

	//for (int i = 0; i < numTotalIter; i++)
	//	printf("r(%d)\t= %lf\n", i, listaNormaResiduos[i]);
	FREE(listaNormaResiduos);
}


void LCD(LinearSystem* ls, int kMax, int lMax, double tolerancia) {

	printf("Resolvendo pelo LCD...\n\n");
	int n = ls->A->n;
	MAT* A = ls->A;
	double* b = ls->b;
	double* x = ls->x;
	double** P = (double**) malloc( (kMax+1) * sizeof(double*));
	double** Q = (double**) malloc( (kMax+1) * sizeof(double*));
	for (int i = 0; i < kMax+1; i++) {
		P[i] = alocarVetor(n);
		Q[i] = alocarVetor(n);
	}
	double* residuo = copiarVetor(b,n); // r0 = b - Ax
	double normaResiduo = normaEuclidiana(residuo, n);
	double alpha, beta, epsilon = tolerancia * normaResiduo;

	copiarValores(residuo, P[0], n); // P para Pt*A*P != 0
	produtoMatVet(A, P[0], Q[0]); // Q = AP

	int k = 0;
	while (normaResiduo > epsilon && k < kMax) {

		alpha = produtoEscalarVetVet(P[k], residuo, n);
		alpha /= produtoEscalarVetVet(P[k], Q[k], n);

		somarComProdutoEscalarVet(x, alpha, P[k], n);
		somarComProdutoEscalarVet(residuo, (-alpha), Q[k], n);

		normaResiduo = normaEuclidiana(residuo, n);
		printf("rho %d = %lf\n", k, normaResiduo);

		copiarValores(residuo, P[k+1], n);
		produtoMatVet(A, P[k+1], Q[k+1]);

		for (int i = 0; i <= k; i++) {
			beta = - produtoEscalarVetVet(P[i], Q[k+1], n);
			beta /= produtoEscalarVetVet(P[i], Q[i], n);

			printf("beta: %lf\n", beta);
			somarComProdutoEscalarVet(P[k+1], beta, P[i], n);
			somarComProdutoEscalarVet(Q[k+1], beta, Q[i], n);
		}
		k++;
	}

	printf("Resolvido em %d iteracoes.\n", k);
	freeVetor(residuo);
	// Clean P and Q
	for (int i = 0; i < kMax+1; i++) {
		freeVetor(P[i]);
		freeVetor(Q[i]);
	}
	FREE(P);
	FREE(Q);
}



















