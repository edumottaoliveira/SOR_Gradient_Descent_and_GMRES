Passos para a execução do código:

1. Executar o Makefile: Altere a localização para o diretório raiz e compile os arquivos usando o comando "make" no terminal;

2. Entrar com o comando: "./exComp2 -solver y", onde:

	-solver: pode ser:
		- "-sor"
		- "-gmres"
		- "-lcd"
		- "-gc"
	

	y: nome do arquivo onde se encontra a matriz

	Exemplos: 
	> ./exComp2 -sor "bcsstk14.mtx"



