#include <stdlib.h>
#include <stdio.h>
#include "util.h"
#include "csrMatrix.h"
#include "util.h"

/*----------------------------------------------------------------------------
 * Read matrix from file in MM format to a CSR structure
 *--------------------------------------------------------------------------*/
void MATRIX_readCSR (MAT* A, char* p)
{
	int M, N, nz;
	int i, j, row, colunm, elem = 0;
	double value;
	char line[1025];
	FILE* f = fopen(p, "r");

	if (f == NULL) {
		ERROR("Arquivo nao encontrado para leitura.");
		exit(1);
	}

	do
	{
		if (fgets(line,1025,f) == NULL)
			exit(0);
	} while (line[0] == '%');

	sscanf(line,"%d %d %d", &N, &M, &nz);

	/* reseve memory for matrices */
	A->m   = M;
	A->n   = N;
	A->nz  = nz;

	A->AA  = (double *) calloc(nz, sizeof (double));
	A->D   = (double *) calloc(N,  sizeof (double));
	A->JA  = (int    *) calloc(nz, sizeof (int));
	A->IA  = (int    *) calloc(N+1,sizeof (int));

	for (i = 0; i < nz; ++i)
	{
		fscanf(f, "%d %d %lf\n", &colunm, &row, &value);
		A->AA[i]   = value;
		A->JA[i]   = colunm - 1;
		elem      += 1;
		A->IA[row] = elem;
	}

	/* Adjusting IA array */
	for (i = 1; i < N + 1; ++i)
		if (A->IA[i] == 0)
			A->IA[i] = A->IA[i-1];

	/* Diagonal */
	if (M == N) /* square matrix */
	{
		for (i = 0; i < A->n; ++i)
		{
			int k1 = A->IA[i];
			int k2 = A->IA[i+1]-1;
			for (j = k1; j <= k2; ++j)
				if (i == A->JA[j])
					A->D[i] = A->AA[j];
		}
	}
	fclose(f);
}

/*----------------------------------------------------------------------------
 * Free up memory allocated for MAT struct
 *--------------------------------------------------------------------------*/
void MATRIX_clean (MAT* A)
{
	FREE(A->AA);
	FREE(A->JA);
	FREE(A->IA);
	FREE(A->D);
	FREE(A);
}

void MATRIX_print(MAT* matrix, char* description) {
	if (matrix == NULL) {
		printf("CSRMatrix nula, nao definida.\n");
		return;
	}
	printf("\n%s:\n", description);
	printf("tamanho: %d\telementos nao nulos: %d\n", matrix->m, matrix->nz);
	int i = 0, k = 0;
	while (k < matrix->nz) {
		printf("k: %d\n", k);
		int nextRowIndex = matrix->IA[i+1];
		while (k < nextRowIndex) {
			printf("%d\t%d\t%lf\n", i, matrix->JA[k], matrix->AA[k]);
			k++;
		}
		i++;
	}


	/*printf("---\n");
	for (k = 0; k < matrix->n+1; k++) {
		printf("%d ", matrix->IA[k]);
	}*/
}
