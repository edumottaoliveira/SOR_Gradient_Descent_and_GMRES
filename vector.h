#ifndef VECTOR_H
#define VECTOR_H

double* alocarVetor(int n);

void freeVetor(double* v);

void printVetor(double* v, int n, char* nome);

double* copiarVetor(double* v, int n);

void copiarValores(double* vetOrigem, double* vetDestino, int n);

double normaEuclidiana(double* v, int n);

void tocarPonteirosVetores(double* v1, double* v2);

double normaInfinito(double* v, int n);

double maxSubtracaoVetVet(double* v1, double* v2, int n);

void zerarVetor(double* v, int n);

double* gerarVetorUnitario(int n);

double normaEuclidianaAoQuadrado(double* v, int n);

double produtoEscalarVetVet(double* v1, double* v2, int n); // v1 . v2

void somarComProdutoEscalarVet (double* v1, double escalar, double* v2, int n); // v1 + scalar*v2

void dividirVetorPorConstante(double* v, double constante, int n);

void somarVetComVet(double* v1, double* v2, double* resultado, int n);

void subtrairVetDeVet(double* v1, double* v2, double* resultado, int n);

void produtoVetorPorConstante(double* v, double constante, double* resultado, int n);

#endif
