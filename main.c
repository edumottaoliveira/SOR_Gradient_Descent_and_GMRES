/*
 ============================================================================
 Nome      : exercicioComp2.c
 Autor     : Eduardo Motta de Oliveira
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"
#include "util.h"
#include "csrMatrix.h"
#include "csrMatrixSystem.h"
#include "solvers.h"

int main(int argc, char *argv[]) {

	if (argc != 3) {
		printf("\n Erro! Sem arquivo de matriz ou metodo de solucao.");
		printf("\n Modo de usar: ./program -<metodo> <nome_da_matriz> \n");
		return 0;
	}

	/* Checa metodo de solucao: -sor, -gc, -gmres, -lcd */
	if (argv[1] == NULL)
		ERROR("Metodo escolhido eh null");

	char sorStr[] = "-sor";
	char gcStr[] = "-gc";
	char gmresStr[] = "-gmres";
	char lcdStr[] = "-lcd";

	MAT* A = (MAT*) malloc (sizeof(MAT));
	CHECKALLOC(A);
	MATRIX_readCSR (A,argv[2]);
	LinearSystem* ls = criarSistemaComSolUnitaria(A);
	double tol = 0.00000001;
	//double tol = 0.0001;
	int strLen = strlen(argv[2]);
	argv[2][strLen - 4] = '\0';
	printf("Input: %s\n", argv[2]);

	if (strcmp(argv[1], sorStr) == 0) {
		SOR(ls, 500000, 0.9, tol, argv[2]);
	}
	else if (strcmp(argv[1], gcStr) == 0) {
		CG(ls, 30000, tol, argv[2]);
	}
	else if (strcmp(argv[1], gmresStr) == 0) {
		GMRES_Restart(ls, 50, 500000, tol, argv[2]);
	}
	else if (strcmp(argv[1], lcdStr) == 0) {
		LCD_Restart(ls, 50, 40000, tol, argv[2]);
	}
	else {
		ERROR("Metodo de solucao nao identificado.\nEscolha entre: sor, gc, gmres ou lcd.");
	}

	//printVetor(ls->x, ls->A->n, "Solucao");
	for (int i = 0; i < 5; i++) {
		printf("\t%0.16lf\n", ls->x[i]);
	}
	freeLinearSystem(ls);

	return EXIT_SUCCESS;
}

