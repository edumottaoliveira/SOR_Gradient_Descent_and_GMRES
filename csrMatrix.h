#ifndef CSRMATRIX_H
#define CSRMATRIX_H

typedef struct
{
	double*     AA; //val
	double*      D;
	int*        JA; //col
	int*        IA; //pointer
	int     m,n,nz;
} MAT;

void MATRIX_readCSR (MAT* A, char* p);

void MATRIX_clean (MAT* A);

void MATRIX_print(MAT* matrix, char* description);

double MATRIX_getValue(int i, int j, MAT* matrix);

MAT* transformUpTriagularInSymetricMatrix(MAT* A);

#endif
