#ifndef SOLVERS_H
#define SOLVERS_H

#include "csrMatrixSystem.h"

void SOR(LinearSystem* ls, int iMax, double w, double tolerancia, char* nomeMatriz);

void CG(LinearSystem* ls, int kMax, double tolerancia, char* nomeMatriz);

void GMRES(LinearSystem* ls, int iMax, int lMax, double tolerancia);

void GMRES_Restart(LinearSystem* ls, int iMax, int lMax, double tolerancia, char* nomeMatriz);

void LCD(LinearSystem* ls, int kMax, int lMax, double tolerancia);

void LCD_Restart(LinearSystem* ls, int kMax, int lMax, double tolerancia, char* nomeMatriz);

#endif
