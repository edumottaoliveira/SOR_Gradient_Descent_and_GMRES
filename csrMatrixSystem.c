#include <stdio.h>
#include "csrMatrixSystem.h"
#include "vector.h"
#include "util.h"


LinearSystem* criarSistemaComSolUnitaria(MAT* A) {
	LinearSystem* ls = (LinearSystem*) malloc (sizeof(LinearSystem));
	ls->A = A;
	double* b = alocarVetor(A->m);

	int i = 0, k = 0;
	while (k < A->nz) {
		int indiceProxLinha = A->IA[i+1];
		//k = A->IA[i];
		double soma = 0.0;
		while (k < indiceProxLinha) {
			soma += A->AA[k];
			k++;
		}
		b[i] = soma;
		i++;
	}

	ls->b = b;
	double* x = alocarVetor(A->m);
	ls->x = x;
	return ls;
}

void freeLinearSystem(LinearSystem *ls) {
	MATRIX_clean(ls->A);
	FREE(ls->b);
	FREE(ls->x);
	FREE(ls);
}

void printLinearSystem(LinearSystem *ls) {
	MATRIX_print(ls->A,"A");
	printVetor(ls->b, ls->A->m, "b");
	printVetor(ls->x, ls->A->m, "x");
}

// A x v
void produtoMatVet(MAT* A, double* v, double* resultado) {
	int valoresNaoNulos = A->nz;
	int i = 0, k = 0;
	while (k < valoresNaoNulos) {
		int indiceProxLinha = A->IA[i+1];
		double soma = 0.0;
		while (k < indiceProxLinha) {
			int j = A->JA[k];
			soma += A->AA[k] * v[j];
			k++;
		}
		resultado[i] = soma;
		i++;
	}
}



/*
// result = v1 + v2 . v3
void getSumdoubleWithProductdoubledouble (double* result, double* v1, double* v2, double* v3) {
	int n = v1->size;
	for (int i = 0; i < n; i++) {
		result->val[i] =  v1->val[i] + (v2->val[i] * v3->val[i]);
	}
}

// v(t) x M x v
double getCrossProductTrasposeddoubleMatrixdouble(double* v, MAT* M) {
	double* productMv = allocatedouble(v->size);
	computeProductMatrixdouble(productMv, M, v);
	double result = getDotProductdoubles(v,productMv);
	freedouble(productMv);
	return result;
}
*/


